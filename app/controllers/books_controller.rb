class BooksController < ApplicationController

  def index
    goodreads = Goodreads.new(Goodreads.configuration)
    @read_books = goodreads.shelf(ENV['goodreads_id'], 'read', sort: 'date_read', per_page: 200)
  end

end
