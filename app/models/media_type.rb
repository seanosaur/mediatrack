class MediaType < ApplicationRecord
  has_many :items
end
