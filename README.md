# MediaTrack

## Models

### _Item_
* `name:string`
* `image_url:string`
* `media_type_id:integer`
* `user_id:integer`

### _MediaType_
* `name:string`

### _User_
* Devise with logins for:
  * Goodreads
  * TVShowTime?

## Associations
* Item `belongs_to` MediaType
* Item `belongs_to` User
* MediaType `has_many` Items
* User `has_many` Items
