Goodreads.configure(
  api_key: ENV["goodreads_key"],
  api_secret: ENV["goodreads_secret"]
)
