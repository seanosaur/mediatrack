Rails.application.routes.draw do
  get 'books/index'

  root 'books#index'
  resources :items
  get 'search', to: 'items#search'
  devise_for :users
end
